package lab5;


public class Enumerations2 {

	public enum ComplexEnum {
		FIRST(1), THIRD(3), TENTH(10);
		private int index = 0;

		private ComplexEnum(int index) {
			this.index = index;
		}

		public int getIndex() {
			return index;
		}

		public void print() {
			System.out.println("index is set in ComplexEnumerations at " + index);
		}
	}

	public static void main(String[] args) {
		ComplexEnum complex = ComplexEnum.TENTH;
		complex.print();

		for (ComplexEnum c : ComplexEnum.values()) {
			switch (c) {
				case FIRST:
				case THIRD:
					System.out.println("ComplexEnum poate fi 1 sau 3");
					break;
				default:
					System.out.println("ComplexEnum este 10");
			}
		}
		System.out.println("pozitia lui THIRD este " + ComplexEnum.valueOf("THIRD").getIndex());
	}

}


