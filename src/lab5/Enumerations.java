package lab5;

import java.util.EnumSet;

public class Enumerations {

	public enum Day{
		SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY
	}
	public static void main(String []args){
		for(Day d : EnumSet.range(Day.MONDAY, Day.WEDNESDAY)){
			System.out.println(d);
		}
	}

}


