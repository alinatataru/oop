package lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TaskArrayDeque {
	public static void main(String[] args){
		Deque<Integer> deque = new ArrayDeque<>();
		HashSet<Integer> set = new HashSet<>();
		int m =3;
		int max= Integer.MAX_VALUE;
		try(Scanner sc= new Scanner(new File("src/resource/test"))){
			while (sc.hasNext()){
				Integer input = Integer.parseInt(sc.next());
				deque.add(input);
				set.add(input);

				if(deque.size()==m){
					if(set.size()>max){
						max= set.size();
					}
					int first = deque.remove();
					if(!deque.contains(first)){
						set.remove(first);
					}
				}
			}
		}catch (FileNotFoundException e){
			e.printStackTrace();
		}
		System.out.println(max);
	}
}
