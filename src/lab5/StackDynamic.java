package lab5;

import lab6.StackDynamicExceptions;

public class StackDynamic<T extends Number> {
	private T info;
	private StackDynamic<T> urm;
	private StackDynamic<T> vf;
	private int noOfElements;
	private int maxNoOfElements = 10;

	public StackDynamic() {
		info = null;
		urm = null;
		vf = this;
	}

	public StackDynamic(T elem) {
		this.info = elem;
		urm = null;
		vf = this;
		noOfElements = noOfElements + 1;
	}

	public StackDynamic(T elem, int maxNoOfElements) {
		this.info = elem;
		urm = null;
		vf = this;
		noOfElements = 0;
		if (maxNoOfElements < 1) {
			throw StackDynamicExceptions.tooManyElements();
		} else {
			this.maxNoOfElements = maxNoOfElements;
		}

	}

	public void push(T elem) {
		if (noOfElements > maxNoOfElements) {
			throw StackDynamicExceptions.stackIsFull();
		}
		if(vf.noOfElements == 0 ){
			vf.noOfElements = 1;
			vf.urm = null;
			vf.info = elem;
		}else{
			StackDynamic<T> p = new StackDynamic<>();
			p.info=elem;
			p.noOfElements = vf.noOfElements +1;
			p.urm=vf;
			vf=p;

		}
//		StackDynamic<T> p = new StackDynamic<>();
//		p.info = elem;
//		p.noOfElements = vf.noOfElements +1;
////		vf.noOfElements = vf.noOfElements + 1;
//		vf.urm = p;
//		vf = p;

	}

	public T pop() {
		if (vf == null) {
			throw StackDynamicExceptions.stackIsEmpty();
		} else {
			T aux = vf.info;
			vf = vf.urm;
//			vf = vf.urm;
//			noOfElements = noOfElements - 1;
			return aux;
		}
	}

	public T peek() {
		if (vf == null) {
			throw StackDynamicExceptions.stackIsEmpty();
		} else return vf.info;
	}

	public Boolean isFull() {
		return noOfElements == maxNoOfElements;
	}

	public boolean isEmpty() {
		if (noOfElements == 0) {
			return true;
		} else {
			return false;
		}
	}

	public String toString() {
		StringBuilder output = new StringBuilder("StackDynamic[");
		for (StackDynamic<T> i = vf; i != null; i = i.urm) {
			output.append(i.info.toString());
			if (i.urm != null) {
				output.append(",");
			}
		}
		output.append("]");
		return output.toString();
	}
}
