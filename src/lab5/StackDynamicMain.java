package lab5;

public class StackDynamicMain {
	public static void main(String[] args) {
		StackDynamic<Integer> stack1 = new StackDynamic<>(40);
		StackDynamic<Double> stack2 = new StackDynamic<>(9.8, 2);

		stack1.push(50);
		stack1.push(60);
		stack1.pop();
		System.out.println(stack1.peek());
		System.out.println(stack1.isFull());
		System.out.println(stack1);

		stack2.push(5.6);
		stack2.push(3.4);
		stack2.pop();
		System.out.println(stack2.peek());
		System.out.println(stack2.isFull());
		System.out.println(stack2);


	}
}
