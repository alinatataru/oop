package lab5;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

public class taskHashMap {

	private static Predicate<String> isNotGb() {
		return key -> !key.equals("GB");
	}

	public static void main(String[] args) {
		Map<String, String> hmCountry = new HashMap<>();
		hmCountry.put("DE", "Germany");
		hmCountry.put("RO", "Romania");
		hmCountry.put("ES", "Spain");
		hmCountry.put("FI", "Finland");
		hmCountry.put("GR", "Greece");
		hmCountry.put("JM", "Jamaica");
		hmCountry.put("MV", "Maldives");
		hmCountry.put("MC", "Monaco");
		hmCountry.put("PT", "Portugal");
		hmCountry.put("GB", "Great Britain");

		hmCountry.forEach((countryCode, countryName) -> System.out.println(countryName + " has " + countryCode + " countryCode"));

		hmCountry.remove("GB");

		System.out.println("Map after removing GB");

		hmCountry.forEach((countryCode, countryName) -> System.out.println(countryName + " has " + countryCode + " countryCode"));

		System.out.println();
		System.out.println();

		hmCountry.keySet()
				.stream()
				.sorted(String::compareTo)
				.forEach(countryCode -> System.out.println(hmCountry.get(countryCode) + " has " + countryCode + " countryCode"));
		;

	}


}
