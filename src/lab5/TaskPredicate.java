package lab5;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class TaskPredicate {

	public static Predicate<Integer> isEven() {
		return p -> p % 2 == 0;
	}

	public static Predicate<Integer> isPositive() {
		return e -> e >= 0;
	}

	public static Predicate<Integer> isPalindrome() {
		return e ->{
			e=Math.abs(e);
			int num = e;
			int reverseNum = 0;
			while (e > 0) {
				int remainder = e % 10;
				reverseNum = reverseNum * 10 + remainder;
				e = e / 10;
			}
			return reverseNum == num;
		};
	}

	public static Predicate<Integer> isPrime() {
		return e -> {
			boolean prim = true;
			for (int i = 2; i < e / 2; i++) {
				if (e % i == 0) {
					prim = false;
					break;
				}
			}
			return prim;
		};
	}


	public static void main(String[] args) {
		List<Integer> integerList = Arrays.asList(-22, 22, 13, 457,  9876, 6776);

		integerList.forEach(el -> System.out.print(el+" "));
		System.out.println();

		System.out.println("No of even elements " +
						integerList.stream()
//						.filter(integer -> integer%2==0)
								.filter(isEven())
								.count()
		);

		System.out.println("No of positive elements " +
				integerList.stream()
						.filter(isPositive())
						.count()
		);

		System.out.println("No of prime elements " +
				integerList.stream()
						.filter(isPrime())
						.count()
		);

		System.out.println("No of palindrom elements " +
				integerList.stream()
						.filter(isPalindrome())
						.count()
		);
	}
}
