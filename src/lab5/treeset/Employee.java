package lab5.treeset;

import java.util.function.Predicate;

public class Employee implements Comparable<Employee> {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Employee employee) {
		if (employee.id > this.id) {
			return -1;
		} else if (employee.id == this.id) {
			return 0;
		} else return 1;

		//ori putem face: return Integer.compare(employee.id, this.id);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("Employee{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append('}');
		return sb.toString();
	}


}
