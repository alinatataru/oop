package lab5.treeset;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;

public class MainTreeSet {

	public static Predicate<Employee> removeCarlos() {
		return p -> !p.getName().equals("Carlos");
	}

	public static void main(String[] args) {

		Employee employee1 = new Employee();
		employee1.setId(25);
		employee1.setName("alina");
		Employee employee2 = new Employee();
		employee2.setId(98);
		employee2.setName("Bianca");
		Employee employee3 = new Employee();
		employee3.setId(83);
		employee3.setName("Alina");
		Employee employee4 = new Employee();
		employee4.setId(8);
		employee4.setName("Carlos");

		Set<Employee> treeSet = new TreeSet<>();
		treeSet.add(employee1);
		treeSet.add(employee2);
		treeSet.add(employee3);
		treeSet.add(employee4);
		System.out.println(treeSet);

		Set<Employee> treeSetName = new TreeSet<>(new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});

//		Set<Employee> treeSetName = new TreeSet<>((o1, o2) -> o1.getName().compareTo(o2.getName()));
//		Set<Employee> treeSetName = new TreeSet<>(Comparator.comparing(Employee::getName));

		treeSetName.add(employee1);
		treeSetName.add(employee2);
		treeSetName.add(employee3);
		treeSetName.add(employee4);
		System.out.println(treeSetName);


		treeSetName.stream().filter(removeCarlos()).forEach(System.out::print);

//		treeSetName.stream().filter(employee -> employee.getName().equals("Carlos")).forEach(System.out::println);

		Set<Employee> treeSetIdName = new TreeSet<>(new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				int compareIdResult = Integer.compare(o1.getId(), o2.getId());
				if (compareIdResult == 0) {
					return o1.getName().compareTo(o2.getName());
				} else return compareIdResult;
			}
		});

	}
}
