package lab5;

import java.util.Optional;

public class Lambda {
	public static void main(String[] args) {
		ELev eLev = new ELev();
		eLev.setName("asdsad");
//		if(eLev.getName()!= null){
//			System.out.println(eLev.getName().length());
//		}

		Optional.ofNullable(eLev.getName())
				.filter(s -> s.equals("Andrei"))
				.ifPresent(name-> System.out.println(name));

	}

	static class ELev{
		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}
}
