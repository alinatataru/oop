package lab1.university;

public class Student {
	private String name;
	private int age;
	private String idNumber;

	public Student() {
	}

	public Student(String name, int age, String idNumber) {
		this.age = age;
		this.idNumber = idNumber;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Name " + name + ", age " + age + " idNumber " + idNumber;
	}

	public void setName(String name){
		this.name=name;
	}

	public String getName(){
		return name;
	}

	public void setAge(int age){
		this.age=age;
	}
	public int getAge(){
		return age;
	}

	public void setIdNumber(String idNumber){
		this.idNumber=idNumber;
	}
	public String getIdNumber(){
		return idNumber;
	}

}
