package lab1.university;

public class Main {
	public static void main(String []args){
		Student student1 = new Student();
		Student student2 = new Student("Victor", 12, "1940303243564");
		Student student3 = new Student("Florin", 12,"1990404239576" );

		student1.setAge(34);
		student1.setIdNumber("1950101285345");
		student1.setName("Popescu");


		System.out.println(student1.getAge());
		System.out.println(student2);
		System.out.println(student3);
	}
}
