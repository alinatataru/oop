package fai;

import java.util.Scanner;
//Approach: The idea is based on the following observations:
//		A solution exists if and only if B is divisible by GCD(A, N) i.e B%GCD(A, N)=0.
//		The number of solutions for X (mod N) is GCD(A, N).
class Tema2 {
	public static long[] algoritmulExtinsEuclid(long a, long b) {
		//acest algoritm gaseste cmmdc pt 2 nr:  ax + by = gcd(a, b)
		// Cazul de baza
		if (a == 0) {
			return new long[]{b, 0, 1};
		} else {

			// Stocheaza rezultatul metodei recursive
			long x1 = 1, y1 = 1;
			long gcdy[] = algoritmulExtinsEuclid(b % a, a);
			long gcd = gcdy[0];
			x1 = gcdy[1];
			y1 = gcdy[2];

			// X si y iau valorile rezultate din apelul recursiv
			long y = x1;
			long x = y1 - (long) Math.floor(b / a) * x1;

			return new long[]{gcd, x, y};
		}
	}

	// Metoda pentru a calcula solutiile distincte pt ecuatia: ax = c (mod n)
	// C = c + (n - b) calculat in main
	public static void congruentaLiniara(long A, long C, long N) {
		A = A % N;
		C = C % N;

		long u = 0, v = 0;

		// calculam valoarea d (cmmd) si u
		long[] vectorDivizori = algoritmulExtinsEuclid(A, N);
		long d = vectorDivizori[0];
		u = vectorDivizori[1];
		v = vectorDivizori[2];


		if (d==1) {
			System.out.println("Decizie: (∃) clasa " + A + " inversabilă in Z" + N);
		} else {
			System.out.println("Decizie: (∄) clasa " + A + " inversabilă  in Z" + N);
		}

		// Nu exista solutii pentru ca C nu este divizibil cu cel mai mare divizor comun intre A si N.
		if (C % d != 0) {
			System.out.println("Nu exista solutie");
			return;
		}

		// Numarul de solutii pentru X(mod N) este dat de cel mai mare divizor comun ( cmmdc ) intre A si N
		//               --> nrSolutii = cmmdc (A N)
		// deci cunoastem de la inceput nr de solutii posibil
		//ex: ecuatia simplificata 15x=10 -->nr de solutii = cmmdc(15, 20] = 5

		// Prima solutie
		long x0 = (u * (C / d)) % N;
		if (x0 < 0) x0 += N;

		//Afiseaza solutia ecuatiei in functie de numarul de solutii
		if (d==1){
			//Afiseaza solutia unica
			System.out.print("Solutia ecuatiei: x=" + x0);
		} else {
			// Afiseaza multimea de solutii
			System.out.print("Solutia ecuatiei: x ∈ { ");
			for (long i = 0; i <= d - 1; i++) {
				long solutie = (x0 + i * (N / d)) % N;
				System.out.print(solutie + " ");
			}
			System.out.print("}");
		}

	}


	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Ecuatia se rezolva in Z: ");
		int n = sc.nextInt();
		System.out.println("Ecuatia are forma ax+b=c in Z"+n);
		System.out.println("Introduceti a din ecuatie:");
		int a = sc.nextInt();
		System.out.println("Introduceti b din ecuatie:");
		int b = sc.nextInt();
		System.out.println("Introduceti c din ecuatie:");
		int c = sc.nextInt();

		//Simplificam ecuatia eliminand (prin calcul) b
		//valAdd este numarul pe care trebuie sa il adunam la b ca impartindu-l la n(Z) sa dea restul 0 in Z-ul respectiv
		//ex in Z20: 12x+11=5 -->n-b: 20-11=9 --> valAdd=9 --> c=5+9
		//           12x=14
		//            Ax=C

		int valAdd = n-b;
		c=c+valAdd;

		congruentaLiniara(a, c, n);
	}
}
