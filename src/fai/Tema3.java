package fai;

import java.util.Scanner;

import static java.lang.Math.pow;

public class Tema3 {

	static int a;
	static int m;
	static int n;

	public static int cmmdc(int a, int b) {
		while (a % b != 0) {
			int r = a % b;
			a = b;
			b = r;
		}
		return b;
	}

	public static boolean estePrim(int x) {
		if (x < 2 || (x > 2 && x % 2 == 0))
			return false;
		for (int d = 3; d * d <= x; d += 2)
			if (x % d == 0)
				return false;
		return true;
	}

	// TODO check method
	public static int indicatorulEuler(int p) {
		int fi = 1;
		int k = 2;
		if (estePrim(p))
			return p - 1;
		while (p > 1) {
			int putere = 0;
			while (p % k == 0) {
				p /= k;
				putere++;
			}
			if (putere > 0)
				fi *= (pow(k, putere) - pow(k, putere - 1));
			k++;
		}
		return fi;
	}


	public static void citireDate() {
		Scanner sc = new Scanner(System.in);
		System.out.println("a= ");
		a = sc.nextInt();

		while (!estePrim(a)) {
			System.out.println("a trebuie sa fie numar prim! ");
			System.out.println("a= ");
			a = sc.nextInt();
		}

		System.out.println("m= ");
		m = sc.nextInt();
		System.out.println("n= ");
		n = sc.nextInt();


	}


	public static void afisareIndicatorulEuler(int n) {
		System.out.println("fi(" + n + ")=" + indicatorulEuler(n));
	}

	public static int algoritmCalculRest(int a, int m, int n) {
		int fi = indicatorulEuler(n);
		int putere = m % fi;
		int rest = 1;
		for (int i = 1; i <= putere; i++) {
			rest *= a;
			rest %= n;
		}
		return rest;
	}

	// TODO check method
	public static int calculRest(int a, int m, int n) {
		int putere = 0;
		if (cmmdc(a, n) == 1) {
			afisareIndicatorulEuler(n);
			return algoritmCalculRest(a, m, n);
		} else {
			while (n % a == 0) {
				n /= a;
				putere++;
			}
			m -= putere;
		}
		afisareIndicatorulEuler(n);
		return (int) (algoritmCalculRest(a, m, n) * pow(a, putere));
	}

	public static void afisareRest(int a, int m, int n) {
		System.out.println("Restul impartirii (" + a + "^" + m + " : " + n + ") este " + calculRest(a, m, n) );
	}

	public static void main(String[] args) {
		citireDate();
		afisareRest(a, m, n);
	}

}
