package fai;

import java.util.Scanner;

public class Tema1 {
	static int lp;
	static int lq;
	static int k;
	static int rest;

	public static void citireDate() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Introduceti lungimea lui p= ");
		lp = scanner.nextInt();
		System.out.println("Introduceti lungimea lui q= ");
		lq = scanner.nextInt();
		System.out.println("Introduceti (putere) k= ");
		k = scanner.nextInt();
	}

	public static void afisareR() {
		System.out.print("r =  ");
		for (int i = 1; i <= lq; i++) {
			System.out.print("a" + i);
		}
		System.out.println();
	}

	public static void concatenare() {
		int putere = k;
		int contor = 0;
		while (putere * lq < lp) {
			putere = putere + k;
			contor = contor + 1;
		}
		System.out.println("p este prefix de lungime " + lp + " a lui r^" + putere);
		System.out.println("Au fost necesare "+ contor + " (de) concatenari");
		System.out.println();
	}

	public static void afisareP() {
		concatenare();
		int cat = lp / lq;
		rest = lp % lq;
		System.out.print("p = (r^" + cat + ")");
		for (int i = 1; i <= rest; i++) {
			System.out.print("a" + i);
		}
		System.out.println();


	}

	public static void afisareQ() {
		System.out.print("q = ");
		for (int i = rest + 1; i <= lq; i++) {
			System.out.print("a" + i);
		}
		for (int i = 1; i <= rest; i++) {
			System.out.print("a" + i);
		}
	}

	public static void main(String[] args) {
		citireDate();
		afisareR();
		afisareP();
		afisareQ();

	}
}
