package lab2.inheritance;

public class Main {
	public static void main(String[] args) {
		Car myCar = new Car();
		myCar.honk();
		System.out.println(myCar.brand+ " "+ myCar.getModelName());
	}
}
