package lab2.inheritance;

public class Car extends Vehicle {
	private String modelName = "Mustang";

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getModelName() {
		return modelName;
	}
}
