package lab2.inner_classJava;

public class Main {
	public static void main(String[] args) {
		OuterClass myOuter = new OuterClass();
		OuterClass.innerClass myInner = myOuter.new innerClass();
		System.out.println(myOuter.x + myInner.y);
	}
}
