package lab2.polymorphism;

public class Main {
	public static void main(String[] args) {
		Animal myAnimal = new Animal();
		Animal myCat = new Cat();
		Animal myDog = new Dog();

		myAnimal.animalSound();
		myCat.animalSound();
		myDog.animalSound();
	}
}
