package lab2.circle;

public class Circle {
	private double radius = 1.0;
	private String color = "red";
	private final double PI = 3.14;

	public Circle() {
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public Circle(double radius, String color) {
		this.radius = radius;
		this.color = color;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getRadius() {
		return radius;
	}

	public String getColor() {
		return color;
	}

	@Override
	public String toString() {
		return "Circle[radius " + radius + ", color " + color + "]";
	}

	public double getArea() {
//		return PI * radius * radius;
		return PI * Math.pow(radius, 2);
	}
}
