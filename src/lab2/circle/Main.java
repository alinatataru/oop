package lab2.circle;

public class Main {
	public static void main(String[] args) {
		Circle circle1 = new Circle();
		Circle circle2 = new Circle(5.5);
		Circle circle3 = new Circle(4.4, "white");

		System.out.println(circle1 + ", area= " + circle1.getArea());
		System.out.println(circle2 + ", area= " + circle2.getArea());
		System.out.println(circle3 + ", area= " + circle3.getArea());
		System.out.println();
		Rectangle rectangle1 = new Rectangle();
		Rectangle rectangle2 = new Rectangle(3.0f, 2.0f);
		System.out.println(rectangle1 + ", perimeter= " + rectangle1.getPerimeter() + ", area= " + rectangle1.getArea());
		System.out.println(rectangle2  + ", perimeter= " + rectangle2.getPerimeter() + ", area= " + rectangle2.getArea());



	}
}
