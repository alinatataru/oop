package lab2.employee;

public class Main {
	public static void main(String[] args) {
		Employee employeeA = new Employee(1, "Ana", "Popa",4500);
		Employee employeeB = new Employee(2,"Vladimir", "Toma", 7800);
		System.out.println(employeeA.toString() + "; annual salary= " + employeeA.getAnnualSalary() + ", new salary= " + employeeA.raiseSalary(30));
		System.out.println(employeeB.toString() + "; annual salary= " + employeeB.getAnnualSalary() + ", new salary= " + employeeB.raiseSalary(50));

	}
}
