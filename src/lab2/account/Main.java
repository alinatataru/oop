package lab2.account;

public class Main {
	public static void main(String[] args) {
		Account accountA = new Account("12345678", "Popescu", 6753);
		Account accountB = new Account("87654321", "Ionescu", 8290);
		System.out.println(accountA.toString());
		System.out.println(accountB.toString());
		System.out.println(accountA.credit(100));
		System.out.println(accountB.debit(3));
		System.out.println(accountA.transferTo(accountB, 5));
		System.out.println(accountA.toString());
		System.out.println(accountB.toString());
	}
}
