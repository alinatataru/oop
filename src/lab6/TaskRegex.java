package lab6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TaskRegex {
	public static void main(String[] args) {
		List<String> patternList = new ArrayList<>();
		patternList.add("^\\d+$");
		patternList.add("^([+-]?\\d*\\.?\\d*)$");
		patternList.add("^-\\d+$");
		patternList.add("/^([-+]?\\d*\\.?\\d+)(?:[eE]([-+]?\\d+))?$/");

		try (Scanner sc = new Scanner(new File("src/resource/textRegex")).useDelimiter(System.getProperty("line.separator"))) {
			while (sc.hasNext()) {
				String line = sc.next();
				patternList.forEach(patternString -> {
					Pattern pattern = Pattern.compile(patternString);
					Matcher matcher = pattern.matcher(line);
					System.out.println(line + " matches pattern " + pattern + " " + matcher.matches());

				});
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
