package lab6;

public class StackDynamicExceptions {

	private final static String MAX_ELEM_MSG = "Stack maxNoOfElements must be greater than 0!";
	private final static String FULL_STACK_MSG = "Stack is full";
	private final static String EMPTY_STACK_MSG = "Stack is empty";

	private StackDynamicExceptions() {
	}

	public static RuntimeException tooManyElements() {
		return new RuntimeException(MAX_ELEM_MSG);
	}

	public static RuntimeException stackIsFull() {
		return new RuntimeException(FULL_STACK_MSG);
	}

	public static RuntimeException stackIsEmpty() {
		return new RuntimeException(EMPTY_STACK_MSG);
	}

}
