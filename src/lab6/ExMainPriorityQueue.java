package lab6;

import java.util.Comparator;
import java.util.PriorityQueue;

public class ExMainPriorityQueue {
	public static void main(String[] args) {
		Comparator<ExEmployeePriorityQueue> nameComparator = Comparator.comparing(ExEmployeePriorityQueue::getName);
		PriorityQueue<ExEmployeePriorityQueue> priorityQueue = new PriorityQueue<>(nameComparator);
		priorityQueue.add(new ExEmployeePriorityQueue(31L, "DDD"));
		priorityQueue.add(new ExEmployeePriorityQueue(11L, "AAA"));
		priorityQueue.add(new ExEmployeePriorityQueue(61L, "EEE"));
		priorityQueue.add(new ExEmployeePriorityQueue(41L, "CCC"));
		priorityQueue.add(new ExEmployeePriorityQueue(51L, "BBB"));
		priorityQueue.add(new ExEmployeePriorityQueue(21L, "FFF"));

		priorityQueue.stream().forEach(System.out::println);
	}
}
