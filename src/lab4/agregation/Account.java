package lab4.agregation;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Account {
	private int id;
	private Customer customer;
	private double balance;
	private final DecimalFormat decimalFormat = new DecimalFormat("#.##");

	public Account() {
	}

	public Account(int id, Customer customer, double balance) {
		this.id = id;
		this.customer = customer;
		this.balance = balance;
	}

	public int getId() {
		return id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		decimalFormat.setRoundingMode(RoundingMode.CEILING);
		return customer + "balance=$" + decimalFormat.format(balance);
	}

	public String getCustomerName() {
		return customer.getName();
	}

	public Account deposit(double amount) {
		return new Account(id, customer, balance + amount);
	}

	public Account withdraw(double amount) {
		if (balance >= amount) {
			return new Account(id, customer, balance - amount);
		} else {
			System.out.println("Amount withdraw exceeds the current balance!");
			return this;
		}
	}
}
