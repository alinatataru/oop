package lab4.agregation;

public class Main {
	public static void main(String[] args) {
		MyPoint[] myPoints = new MyPoint[10];
		for (int i = 0; i < 10; i++) {
			myPoints[i] = new MyPoint(i + 1, i + 1);
		}
		for (int i = 0; i < 10; i++) {
			System.out.println(myPoints[i].distance());
		}
		MyPoint myPoint1= new MyPoint(1,1);
		MyPoint myPoint2= new MyPoint(3,7);
		System.out.println();
		System.out.println(myPoint1.distance(myPoint2));
		System.out.println(myPoint1.distance(myPoint2.getX(),myPoint2.getY()));

	}


}
