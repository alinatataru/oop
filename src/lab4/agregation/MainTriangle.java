package lab4.agregation;

public class MainTriangle {
	public static void main(String[] args) {
		MyTriangle triangle1 = new MyTriangle(new MyPoint(), new MyPoint(9, 19), new MyPoint(9, 19));
		MyTriangle triangle2 = new MyTriangle(10, 9, 3, 7, 8, 12);
		System.out.println("The perimeter of triangle1 is " + triangle1.getperimeter());
		System.out.println("Triangle1 is " + triangle1.getType());
		System.out.println("The perimeter of triangle2 is " + triangle2.getperimeter());
		System.out.println("Triangle 2 is " + triangle2.getType());

	}


}

