package lab4.agregation;

public class MyPoint {
	private int x = 0;
	private int y = 0;


	public MyPoint() {
	}

	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int[] getXY() {
		return new int[]{x, y};
//		int[] v=new int[2];
//		v[0]=x;
//		v[1]=y;
//		return v;
	}

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

	public double distance(int x, int y){
		return Math.sqrt((y - this.y) * (y - this.y) + (x - this.x) * (x - this.x));
	}
	public double distance (MyPoint myPoint){
		return Math.sqrt((myPoint.getY() - y) * (myPoint.getY() - y) + (myPoint.getX() - x) * (myPoint.getX() - x));
	}
	public double distance (){
		return Math.sqrt((0 - y) * (0 - y) + (0 - x) * (0- x));
	}
}
