package lab4.agregation;

public class MainCustomer {
	public static void main(String[] args) {
		Customer customer1 = new Customer(1, "Ana", 15);
		Customer customer2 = new Customer(2, "Andrei", 40);
		Invoice invoiceC1 = new Invoice(1, customer1, 400);
		Invoice invoiceC2 = new Invoice(2, customer2, 400);

		System.out.println(customer1 + " amount after discount is " + invoiceC1.getAmountAfterDiscount());
		System.out.println(customer2 + " amount after discount is " + invoiceC2.getAmountAfterDiscount());

		Account ac1 = new Account(1, customer1, 1000);
		Account ac2 = new Account(2, customer2, 100);

		ac1.deposit(3000);
		ac2.deposit(20);

		System.out.println(ac1.withdraw(invoiceC1.getAmountAfterDiscount()));
		System.out.println(ac2.withdraw(invoiceC2.getAmountAfterDiscount()));
		System.out.println(ac1.withdraw(400));
		System.out.println(ac2.withdraw(400));

	}
}
