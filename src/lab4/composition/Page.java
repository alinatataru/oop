package lab4.composition;

public class Page {
	private int pageNamber;
	private long numberOfWords;
	private String footnote;

	public Page() {
	}

	public Page(int pageNamber, long numberOfWords, String footnote) {
		this.pageNamber = pageNamber;
		this.numberOfWords = numberOfWords;
		this.footnote = footnote;
	}

	@Override
	public String toString() {
		return "Page number= " + pageNamber + ", number of words= " + numberOfWords + ", footnote= " + footnote;
	}
}
