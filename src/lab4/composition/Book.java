package lab4.composition;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Book {
	private String name;
	private List<Author> authors = new ArrayList<>();
	private double price;
	private int qty = 0;
	private Page page;

	public Book() {
	}

	public Book(String name, List<Author> authors, double price) {
		this.name = name;
		this.authors = authors;
		this.price = price;
	}

	public Book(String name, List<Author> authors, double price, int qty) {
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qty = qty;
	}

	public Book(String name, List<Author> authors, double price, int qty, Page page) {
		this.name = name;
		this.authors = authors;
		this.price = price;
		this.qty = qty;
		this.page = page;
	}

	public Page getPage() {
		return page;
	}

	public String getName() {
		return name;
	}

	public List<Author> getAuthors() {
		return authors;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return "Book[name= " + name + ", " + authors + ", price=" + price + ", qty= " + qty + "]\n" + page;
	}

	public String getAuthorsName() {
		return authors.stream()
				.map(Author::getName)
				.collect(Collectors.joining(","));

//		String names = "";
//		for (int i = 0; i < authors.size(); i++) {
//			names = names + authors.get(i).getName();
//		}
//		return names;
//

//		String names = "";
//		for (Author author : authors) {
//			names = name + author.getName();
//		}
//		return names;



	}
}

