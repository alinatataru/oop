package lab4.composition;

import java.util.Arrays;

public class Main {
	public static void main(String[] args) {
		Author author1 = new Author("Delia Owens", "delia.owens@gmail.com", 'f');
		Author author2 = new Author("Frank Herbert", "frank.herbert@gmail.com", 'm');
		Page page1 = new Page(300, 67899875, "For years, rumors of the 'Marsh Girl' have haunted Barkley Cove.");
		Page page2 = new Page(608,12987567, "I must not fear. Fear is the mind-killer. I will face my fear.");
		Book book1 = new Book("Where the crawdads sing", Arrays.asList(author1,author2), 55, 5, page1);
		Book book2 = new Book( "Dune", Arrays.asList(author1,author2), 100, 10, page2);

		System.out.println(book1);
		System.out.println();
		System.out.println(book2);
		System.out.println();
		System.out.println(book1.getAuthorsName());
		System.out.println(book2.getAuthorsName());
	}
}
