package lab4.composition;

public class Author {
	private String name;
	private String email;
	private char gender;

	public Author() {
	}

	public Author(String name, String email, char gender) {
		this.name = name;
		this.email = email;
		if (gender == 'm' || gender == 'f') {
			this.gender = gender;
		} else {
			throw new RuntimeException("Gender must be m or f");
		}
	}

	public String getName() {
		return name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	public char getGender() {
		return gender;
	}

	public String toString() {
		return "Author[name= " + name + ", email= " + email + ", gender= " + gender + "]";
	}
}
