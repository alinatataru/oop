package lab3.task1;

public class Square extends Rectangle {

	public Square() {
	}

	public Square(double side) {
		super(side, side);
	}

	public Square(double side, String color, boolean filled) {
		super(side, side, color, filled);
	}

	public double getSide() {
		return super.getWidth();
	}

	public void setSide(double side) {
		super.setWidth(side);
	}

	@Override
	public void setWidth(double side) {
		super.setWidth(side);
	}

	@Override
	public void setLength(double side) {
		super.setLength(side);
	}

	@Override
	public String toString() {
		return "Square with side " + super.getWidth() + " and color " + super.getColor() + " is "+(isFilled() ? "filled." : "not filled.");
	}
}
