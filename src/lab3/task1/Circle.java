package lab3.task1;

public class Circle extends Shape {
	private double radius = 1.0;

	public Circle() {
	}

	public Circle(double radius) {
		this.radius = radius;
	}

	public Circle(double radius, String color, boolean filled) {
		super(color, filled);
		this.radius = radius;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getArea(double radius) {
		System.out.println("Area is = ");
		return Math.PI * Math.pow(radius, 2);
	}

	public double getPerimeter(double radius) {
		System.out.println("Perimeter is  ");
		return 2 * Math.PI * radius;
	}

	@Override
	public String toString() {
		return "Circle with radius " + radius;
	}
}
