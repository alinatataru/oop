package lab3.task2;



public class Main {
	public static void main(String[] args) {
		Square sq = new Square(6, "red", true);
		System.out.println(sq);

		Rectangle rct = new Rectangle(4,8, null, false);
//		Rectangle rct = new Rectangle(4,10);
//		rct.setFilled(false);
		System.out.println(rct);

		Circle circle = new Circle(9);
		System.out.println(circle);
	}
}
