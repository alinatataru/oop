package lab3.task2;

public class Rectangle extends Shape {
	private double width = 1.0;
	private double length = 1.0;

	public Rectangle() {
	}

	@Override
	public double getArea() {
		return width*length;
	}

	@Override
	public double getPerimeter() {
		return (2*length)+(2*width);
	}

	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}

	public Rectangle(double width, double length, String color, boolean filled) {
		super(color, filled);
		this.width = width;
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getArea(double length, double width) {
		System.out.println("Area is = ");
		return length * width;
	}

	public double getPerimeter(double length, double width) {
		System.out.println("Perimeter is  ");
		return (2 * length) + (2 * width);
	}

	@Override
	public String toString() {
		return "Rectangle: "+ super.toString() + " with length " + length + " and width " + width;
	}
}
