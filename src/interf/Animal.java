package interf;

 interface Animal {
	void animalSound();

	void sleep();

	default void run() {
		System.out.println("run");
	}

}
