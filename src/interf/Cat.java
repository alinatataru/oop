package interf;

class Cat implements Animal{

	@Override
	public void animalSound() {
		System.out.println("The cat says: meow meow");
	}

	@Override
	public void sleep() {
		System.out.println("Zzzz Zzzz Zzzz");
	}

	@Override
	public void run() {
		Animal.super.run();
	}
}
