package lab5;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackDynamicTest {

	@Test
	public void push() {
		//given
		StackDynamic<Double> stack = new StackDynamic<>();

		//when
		stack.push(9.0);

		//then
		assertNotNull(stack);

	}

	@Test
	public void pop() {
		//given
		StackDynamic<Double> stack = new StackDynamic<>();
		Double lastElem = 8.8;
		Double firstElem = 5.4;

		stack.push(firstElem);
		stack.push(lastElem);

		//when
		Double removedElem = stack.pop();

		//then
		assertEquals(lastElem, removedElem);
		assertFalse(stack.isEmpty());
	}

	@Test
	public void peek() {
		//given
		StackDynamic<Double> stack = new StackDynamic<>();
		Double element = 9.4;
		stack.push(element);

		//when
		Double answer = stack.peek();

		//then
		assertEquals(answer,element);

	}

	@Test
	public void isFull() {
	}

	@Test
	public void isEmptyWhenNoElement() {
		//given
		StackDynamic<Double> stack = new StackDynamic<>();


		//when
		boolean rezultat = stack.isEmpty();

		//then
		assertTrue(rezultat);

	}

	@Test
	public void isNotEmptyWhenNoElement() {
		//given
		StackDynamic<Double> stack2 = new StackDynamic<>();
		stack2.push(3.11);

		//when
		boolean rezultat = stack2.isEmpty();

		//then
		assertFalse(rezultat);
	}
}